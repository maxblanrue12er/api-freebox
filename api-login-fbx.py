import hmac
import time
from apize.apize import Apize

app = Apize('http://mafreebox.freebox.fr/api/v3')

@app.call('/login/session/', method='POST')
def connect_app(app_token, app_id, challenge):
    h = hmac.new(app_token.encode(), challenge, 'sha1')
    password = h.hexdigest()
    data = {'app_id': app_id, 'password': password}
    headers = {'X-Fbx-App-Auth': app_token}
    return {
        'data': data,
        'headers': headers,
        'is_json': True
    }

@app.call('/login/authorize/:id')
def authorize_app(track_id):
    args = {'id': track_id}
    return {'args': args}

@app.call('/login/logout/', method='POST')
def deconnect_app(session_token):
    headers = {'X-Fbx-App-Auth': session_token}
    return {'headers': headers}

def get_session_token(app_token, app_id, track_id):
    response = authorize_app(track_id)
    challenge = response['data']['result']['challenge'].encode()
    conn = connect_app(app_token, app_id, challenge)
    session_token = conn['data']['result']['session_token']
    if debug == 'yes':
        print "Le session token est " + session_token
        print conn['data']['result']['permissions']
    return session_token

@app.call('/airmedia/receivers/Freebox%20Player/', method='POST')
def airmedia(action,Media_Type=None,URL_Media=None):
    if  action == "start":
        if !Media_Type:
            print 'Veuillez indiquer un type de media'
            Media_Type = input("Veuillez ")
        data = {'action': 'start', 'media_type': Media_Type, 'media': URL_Media}
    else :
        data = {'action': 'stop', 'media_type': 'video'}
    headers = {'X-Fbx-App-Auth': session_token}
    return {
        'data': data,
        'headers': headers,
        'is_json': True
                }
        
if __name__ == '__main__':
    app_token = 'OtvDYRZ4/HPWRnrmsaCv9zJqCrvNZqgI0xcXBdD2yRsLlbgDd8Bx7KimFVcSkZT/'
    app_id = 'fr.maxou.testapp'
    track_id = 0
    debug = 'yes'
    print 'Démarage en cours'
    session_token = get_session_token(app_token, app_id, track_id)
        
    if session_token:
        while True:
        	i = input("Quel API voulez vous lancer ?")
        	if (i == "airmedia") :
        		URL = input("Indiquez le lien du media que vous voulez lancer :")
        		airmedia("start",URL)
        	if (i == "airmedia-stop"):
        		airmedia("stop")
        	if (i == "stop") :
        		deconnect_app(session_token)
        		break